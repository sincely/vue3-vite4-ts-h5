import axios from 'axios'
import { ContentTypeEnum, ResultEnum } from '@/enums/requestEnum'
import NProgress from '../progress'
import { showFailToast } from 'vant'
import 'vant/es/toast/style'
// 创建axios实例
const service = axios.create({
  headers: {
    'Content-Type': ContentTypeEnum.FORM_URLENCODED
  },
  timeout: 0,
  baseURL: import.meta.env.VITE_BASE_API // 需自定义
})

// 请求拦截
service.interceptors.request.use(
  (config) => {
    NProgress.start()
    // 发送请求前，可在此携带 token
    // if (token) {
    //   config.headers['token'] = token
    // }
    return config
  },
  (error) => {
    showFailToast(error.message)
    return Promise.reject(error)
  }
)

// 响应拦截

// respone拦截器
service.interceptors.response.use(
  (response) => {
    NProgress.done()
    // 统一处理状态
    const { code, message, result } = response.data
    // 判断请求是否成功
    const isSuccess = result && Reflect.has(response.data, 'code') && code === ResultEnum.SUCCESS
    if (isSuccess) {
      // 需自定义返回异常
      return result
    } else {
      // 处理请求错误
      showFailToast(message)
      return Promise.reject(response.data)
    }
  },
  // 处理处理
  (error) => {
    if (error && error.response) {
      switch (error.response.status) {
        case 400:
          error.message = '错误请求'
          break
        case 401:
          error.message = '未授权，请重新登录'
          break
        case 403:
          error.message = '拒绝访问'
          break
        case 404:
          error.message = '请求错误,未找到该资源'
          break
        case 405:
          error.message = '请求方法未允许'
          break
        case 408:
          error.message = '请求超时'
          break
        case 500:
          error.message = '服务器端出错'
          break
        case 501:
          error.message = '网络未实现'
          break
        case 502:
          error.message = '网络错误'
          break
        case 503:
          error.message = '服务不可用'
          break
        case 504:
          error.message = '网络超时'
          break
        case 505:
          error.message = 'http版本不支持该请求'
          break
        default:
          error.message = `未知错误${error.response.status}`
      }
    } else {
      error.message = '连接到服务器失败'
    }
    return Promise.reject(error)
  }
)

export const http = service
