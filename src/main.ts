import 'virtual:uno.css'
// normalize.css
import 'normalize.css/normalize.css'
// 全局样式
import './styles/index.less'
import { createApp } from 'vue'
import App from './App.vue'
import { store } from './store/index'

import router from './router'
// svg icon
import 'virtual:svg-icons-register'
import './styles/index.less'

const app = createApp(App)

app.use(store)
app.use(router)
app.mount('#app')
