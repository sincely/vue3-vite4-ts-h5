module.exports = {
  '*.{js.ts}s': ['prettier --write', 'eslint --fix'],
  '*.vue': ['prettier --write', 'eslint --fix', 'stylelint --fix'],
  'package.json': ['prettier --write'],
  '*.{css,less}': ['prettier --write', 'stylelint --fix']
}
